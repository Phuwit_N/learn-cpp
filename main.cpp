#include <iostream>
#include <string>


using namespace std;

class People{
private:
    string name;
public:
    People(string name){
        this->name = name;
    }
    string getName(){
        return this->name;
    }
};

int main() {
    People p1("Max");
    cout << p1.getName() << endl;
    return 0;
}